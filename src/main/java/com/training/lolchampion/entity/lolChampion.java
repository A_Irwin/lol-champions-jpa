package com.training.lolchampion.entity;

import javax.persistence.*;

@Entity(name="champions")
public class lolChampion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ChampionID")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="role")
    private String role;

    @Column(name="difficulty")
    private String difficulty;
}
