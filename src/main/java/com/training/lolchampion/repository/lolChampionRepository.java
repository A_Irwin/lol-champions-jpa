package com.training.lolchampion.repository;

import com.training.lolchampion.entity.Shipper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface lolChampionRepository extends JpaRepository<Champions, Long> {
}
